FROM ubuntu:latest

#
# NOTE: ENV affects only in container or RUN command (not in Dockerfile)
#
ENV IS_DOCKER=1

# ENV DEBIAN_FRONTEND=noninteractive is discouraged in Docker FAQ
# see: https://docs.docker.com/engine/faq/#why-is-debian_frontendnoninteractive-discouraged-in-dockerfiles
#
RUN set -eux; \
    apt-get update; \
    apt-get install -y tzdata; \
    ln -sf /usr/share/zoneinfo/Asia/Tokyo /etc/localtime; \
    DEBIAN_FRONTEND=noninteractive dpkg-reconfigure tzdata;  # DEBIAN_FRONTEND supperss dpkg-reconfigure interactive input.

# add user ubuntu
RUN set -eux; \
    groupadd --gid 1000 ubuntu; \
    useradd --gid 1000 -G adm,audio,plugdev,sudo,video --create-home --shell /bin/bash --uid 1000 ubuntu; \
    apt-get install -y --no-install-recommends sudo

# install ubuntu basic tools
RUN set -eux; \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    tree \
    git \
    jq \
    less \
    curl \
    apache2-utils \
    ruby \
    python3-setuptools python3-pip

# install awscli
RUN pip3 install --no-cache-dir awscli

# install other
RUN apt-get install -y --no-install-recommends vim-tiny

# from: https://github.com/vorburger/cloudshell/blob/master/init
# tweak /etc/sudoers so that she can sudo without password..
# .. for Debian:
RUN if [ -f /etc/sudoers.d/README ]; then \
    echo "%sudo   ALL=(ALL:ALL) NOPASSWD: ALL" >/etc/sudoers.d/ubuntu; \
  fi

# https://github.com/sudo-project/sudo/issues/42
# https://bugzilla.redhat.com/show_bug.cgi?id=1773148
RUN echo "Set disable_coredump false" >> /etc/sudo.conf

USER 1000
ENV HOME /home/ubuntu
WORKDIR /home/ubuntu

CMD ["bash"]
